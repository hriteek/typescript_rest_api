import { Request, Response } from "express";
import Book from "../book";

// - GET - /books # returns all books

export const allBooks = (req: Request, res: Response) => {
  let books = Book.find()
    .then(books => {
      res.send(books);
    })
    .catch(err => console.error(err));
};

// - GET - /book/{1} # returns a book with id 1
export const getBook = (req: Request, res: Response) => {
  Book.findById(req.params.id)
    .then(book => {
      res.send(book);
    })
    .catch(err => console.error(err));
};
// - POST - /book # inserts a new book into the table
export const addBook = (req: Request, res: Response) => {
  let book = new Book(req.body);
  book
    .save()
    .then(book => {
      res.send(book);
    })
    .catch(err => console.error(err));
};
// - DELETE - /book/{1} # deletes a book with id of 1
export const deleteBook = (req: Request, res: Response) => {
  Book.deleteOne({ _id: req.params.id })
    .then(() => {
      res.send("successfully deleted");
    })
    .catch(err => console.error(err));
};

// - PUT - /book/{1} # updates a book with id of 1
export const updateBook = (req: Request, res: Response) => {
  Book.findByIdAndUpdate(req.params.id, req.body)
    .then(book => {
      res.send("Successfully updated book");
    })
    .catch(err => console.error(err));
};
