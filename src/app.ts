import * as express from "express";
import * as bodyParser from "body-parser";
import * as bookController from "./controllers/bookController";

const app = express();
app.use(bodyParser.json());

app.set("port", 3000);

app.get("/books", bookController.allBooks);
app.get("/book/:id", bookController.getBook);
app.post("/book", bookController.addBook);
app.delete("/book/:id", bookController.deleteBook);
app.put("/book/:id", bookController.updateBook);

app.listen(app.get("port"), () => {
  console.log(`App is running in http://localhost:${app.get("port")}`);
});
