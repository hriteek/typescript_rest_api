import * as mongoose from "mongoose";

const uri: string = "mongodb://localhost:27017/local";

mongoose.connect(uri, (err: any) => {
  if (err) {
    console.error(err);
  } else console.log("Successfully connected to MongoDB");
});

export const BookSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  }
});

const Book = mongoose.model("Book", BookSchema);
export default Book;
